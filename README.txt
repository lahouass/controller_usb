###################### Projet USB #####################

Étudiants :  Nduwamungu Jean-de-Dieu & Ahouassou Loris


================= Utilisation de notre programme =============

A) L'atmega328p servira à la gestion des Boutons et des Leds

1) Dans le dossier atmega328p/
     a)-Faire make
     b)-Faire make upload
 
Remarque : L'atmega16u2 doit être en mode FTDI



B) L'atmega16u2 fera le pont entre le périphérique et le PC. Il se charge d'établir le dialogue et transmet les différentes informations dans les deux sens.

1) Dans le dossier atmega16u2/lufa-LUFA-170418/PolytechLille/PAD/

      a) Faire make
      b) Court-circuitez les broches RESET et GND de l'ICSP du 16u2
      c) Exécuter la suite de commandes suivante, une par une :
         
         - dfu-programmer atmega16u2 erase
         - dfu-programmer atmega16u2 flash PAD.hex
         - dfu-programmer atmega16u2 reset
       d) Ensuite débranchez puis rebranchez la carte
       

Remarque: À ce niveau, en lançant la commande "lsusb", notre périphérique devrait être visible maintenant sous l'IdVendor -> 2020.

C) Le programme PC récupère la configuration du port USB et ainsi permettre de lire des états des boutons et écrire pour allumer/eteindre les leds.

       a) Faire: gcc -o executable -Wall pc.c -lusb-1.0
       b) ./executable   (être en root sera nécessaire)
       
       
=============== Ce qui fonctionne ==============

Atmega328p

Le code de l'atmega328p fonctionne correctement. Nous récupérons parfaitement les valeurs des 4 boutons et ainsi que celle du joystick (en cliquant dessus) et via le minicom ( minicom -8 -o -b 9600 -D /dev/ttyACM0 ), en cliquant sur les boutons, on peut voir s'afficher les caractères correspondant à leur code ascii respectif.

Pour cela nous avons une fonction d'interruption qui permet d'allumer les LED correspondants dès que l'utilisateur appuie sur un des boutons.
Cela est visible dans le minicom, on peut allumer les leds à l'aide des touches A-B-C-D-E-F, et les étiendre avec a-b-c-d-e-f.

Remarque :
Il faut être en FTDI

Atmega16u2

Dans le programme de l'atmega16u2, nous arrivons à bien configuerer les endpoints ainsi que leurs interfaces. 
Notre IdVendor a été fixé à 0x2020, et notre périphérique est bien visible.

PC

La configuration des interfaces et des endpoints a été bien faite et vous pouvez le voir dans le terminal lors de l'exécution du programme pc.
Aussi, nous avons pu assurer la gestion de la manette qui nous permet ainsi d'afficher l'état des boutons directement dans le terminal (après un appui d'un bouton.
Et pour quitter, nous avons choisi le bouton du Joystick comme bouton de sortie qui, après un clic, déclenche la fermeture correcte du périphérique.


============== Ce qui ne fonctionne pas ============

Nous n'avons tout de même pas pu réussir à allumer les leds via le terminal (après l'exécution du programme pc) et non plus à les éteindre lors de la sortie (après clic du bouton du Joystick).












