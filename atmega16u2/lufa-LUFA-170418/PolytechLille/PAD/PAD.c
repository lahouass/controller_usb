#include "PAD.h"

//Programme Atmega16u2
/* Il Fait le lien entre le PC et la manette (Atmega16u2) via une configuration du périphérique et fait attention à la réception ou l'émission des données.*/

int main(void)
{
  bool ReceptionBoutton = false;
  uint8_t Donnee = 0;
  
  /*Hardware Init*/
  USB_Init();

  Serial_Init(9600,false);
  GlobalInterruptEnable();
  for (;;){
    USB_USBTask();
  
    //Selection de l'endpoint de sortie correspondant aux leds
    Endpoint_SelectEndpoint(EP_PAD_OUT_LED_ADDR);
    //Verification de la possibilité d'écriture ou de lecture avant envoi
    if(Endpoint_IsOUTReceived() && Endpoint_IsReadWriteAllowed()){
      Donnee = Endpoint_Read_8();
      Serial_SendByte(Donnee);
      Endpoint_ClearOUT();
    }
    
    //Selection des endpoints d'entrée correspondants aux boutons
    Endpoint_SelectEndpoint(EP_PAD_IN_BUT_ADDR);

    ReceptionBoutton = Serial_IsCharReceived();
    if(ReceptionBoutton){
      Donnee = Serial_ReceiveByte();
      if(Endpoint_IsINReady() && Endpoint_IsReadWriteAllowed()){
        Endpoint_Write_8(Donnee);
        Endpoint_ClearIN();
      }    
    }
    //Selection pour Joystick
   Endpoint_SelectEndpoint(EP_PAD_IN_BUT_ADDR_JOYSTICK);
    ReceptionBoutton = Serial_IsCharReceived();
    if(ReceptionBoutton){
      Donnee = Serial_ReceiveByte();
      if(Endpoint_IsINReady() && Endpoint_IsReadWriteAllowed()){
        Endpoint_Write_8(Donnee);
        Endpoint_ClearIN();
      }   
  }
}
}


/** Event handler for the USB_ConfigurationChanged event. This is fired when the host sets the current configuration
 *  of the USB device after enumeration, and configures the keyboard device endpoints.
 */
 //Fonction pour creer les endpoints
void EVENT_USB_Device_ConfigurationChanged(void){
  /* Setup PAD Endpoints */
  Endpoint_ConfigureEndpoint(EP_PAD_IN_BUT_ADDR,
                EP_TYPE_INTERRUPT,
                PAD_EP_SIZE,
                1);
  Endpoint_ConfigureEndpoint(EP_PAD_OUT_LED_ADDR,
                EP_TYPE_INTERRUPT,
                PAD_EP_SIZE,
                1);
    Endpoint_ConfigureEndpoint(EP_PAD_IN_BUT_ADDR_JOYSTICK,
                EP_TYPE_INTERRUPT,
                PAD_EP_SIZE,
                1);
}
