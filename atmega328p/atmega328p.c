/*Dans cette partie, nous gérons les boutons et les leds.
Ce programme peut être testé de façon indépendante via le minicom*/

#include <stdio.h>
#include <stdlib.h>
#include <avr/io.h>	
#include <util/delay.h>
#include <avr/interrupt.h>


uint8_t donnee = 0;

#define CPU_FREQ        16000000L       //CPU frequency of 16Mhz

//Initialisation du port série par interruption.
void init_serial(int speed){
  /* Set baud rate */
  UBRR0 = CPU_FREQ/(((unsigned long int)speed)<<4)-1;
  /* Enable transmitter & receiver */
  UCSR0B = (1<<TXEN0 | 1<<RXEN0 | 1<<RXCIE0);
  /* Set 8 bits character and 1 stop bit */
  UCSR0C = (1<<UCSZ01 | 1<<UCSZ00);
  /* Set off UART baud doubler */
  UCSR0A &= ~(1 << U2X0);
}

//Envoi d'un caractère sur le port serie.
void send_serial(unsigned char c){
  loop_until_bit_is_set(UCSR0A, UDRE0); //wait until data register empty
  UDR0 = c;
}

//Recuperation de données sur le port serie.
unsigned char get_serial(void) {
  loop_until_bit_is_set(UCSR0A, RXC0); //wait until data exits
  return UDR0;
}

////////////////////// Gestion des entrées/sorties /////////////////////////


//output_init initialise le PORTB lié aux leds en sortie. 
void output_init(void){
  DDRB |= 0x3F; 			// Toutes les leds sont mises en sortie
}


//output_set allume ou eteindre les leds sur le PORTB 
void output_set(unsigned char valeur){
  if(valeur=='1')PORTB |= 0x3F; 	// '1' pour allumer la led
  else PORTB &= 0xC0;  //'0' pour eteindre la led 
}


//input_init initialise le PORTD lié aux boutons en entrée
void input_init(void){
  DDRD &= 0x83;  	// PIN 2 à 6 comme entrées (D3,D4,D5,D6+joystick)
  PORTD |= 0x7c;
}

//input_get recupère l'etat des 5 boutons.
unsigned char input_get(void){
  return ((~PIND)&0x7c)>>2; /*comparaison de la ~PIND avec les pins actives 0x7c, décalage de 2 vers droite 2 bits de poids faible tjrs 00.*/
}

void init_interruption() 
{
    PCICR = 1<<PCIE2;
    PCMSK2 = (1<<PCINT18) | (1<<PCINT19) | (1<<PCINT20) | (1<<PCINT21) | (1<<PCINT22);
    sei();
}


void Gestion_led(unsigned char valeur)
{
    if(valeur >= 'A' && valeur <= 'F') 
    {
    /*Si on reçoit A, PORTB |= 0b00000001 ; Si on reçoit F, PORTB |= 0b00100000*/
    PORTB |= (1 << (valeur - 'A'));
    } 

    else if (valeur >= 'a' && valeur <= 'f')
    {
      PORTB &= ~(1<< (valeur - 'a'));
    } 

    send_serial(valeur);
}

//Au changement de l'état d'un bouton, l'interruption envoie en série la valeur du PIND 
ISR(PCINT2_vect)
{
    donnee = 0x00; 
    donnee = ((~PIND)&0x7c)>>2;
    donnee |= 0x20;
    send_serial(donnee);
    _delay_ms(500);
}

//Interruption qui allume les leds grâce à ce qu'il reçoit en série
ISR(USART_RX_vect)
{
    unsigned char c = UDR0;
    Gestion_led(c);
} 


//========================================================
//___MAIN PROGRAM___

int main(void){	
    unsigned char valeur,recu;
    
  //Initialisation
    input_init();
    init_interruption();
    init_serial(9600);
    output_init();
 
    int ancienne=-1;
    while(1){
    /* Au changement de l'etat des boutons, on récupère la valeur d'etat globale sur la liaison serie.*/
    valeur=input_get();
   //Et on compare avec 0x20 (car affichage en ascii)
    if(valeur!=ancienne){
        ancienne=valeur;
        send_serial(valeur|0x20); 
    } 
    //À l'arrivée d'un octet (en caractere ascii) sur le port serie, on utilise cet octet pour allumer la led. UCSR0A&(1<<RXC0) réception d'une données.
    if(UCSR0A&(1<<RXC0)){
      recu=get_serial();
      output_set(recu);
    }
    _delay_ms(10);
  }
  return 0;
}

