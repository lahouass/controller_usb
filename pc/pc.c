#include <stdio.h>
#include <stdlib.h>
#include <libusb-1.0/libusb.h>
#include <unistd.h>
#include <sys/wait.h>

struct Save {
  uint8_t num_interface;
  uint8_t num_interruptEp;
};

struct libusb_device_handle *Mydevice; //handle
struct libusb_config_descriptor *Myconfig; //descripteur de configuration


void fermeture(libusb_device_handle **handle)
{
    libusb_close(*handle);
    printf("\n\nPériphérique fermé correctement... Bye !\n\n");
}


void configuration()
{
    libusb_device *device = libusb_get_device(Mydevice);
    //On récupère la premiere configuration du periphérique Mydevice
    int status= libusb_get_config_descriptor(device,0,&Myconfig);
    if(status!=0){perror("libusb_get_config_descriptor"); exit(-1);}
    printf("\nConfiguration du péripherique: %d\n",Myconfig->bConfigurationValue);
}

// La fonction suivante réclame les interfaces et sauvegarde les endpoints. 
void interfaces(struct Save * save,int size_inter)
{
    uint8_t interface_numero=-1;
    
    printf("\n>>>>>> On reclame les interfaces\n\n");
    
    //On désactive le driver du noyau pour autoriser libusb en I/O et réclamer les interfaces
    for(int i=0; i<Myconfig->bNumInterfaces;i++){
        interface_numero=Myconfig->interface[i].altsetting[0].bInterfaceNumber; // On récupère le numero d'interface
        if(libusb_kernel_driver_active(Mydevice,interface_numero)){
        int status=libusb_detach_kernel_driver(Mydevice,interface_numero);
        if(status!=0){perror("libusb_detach_kernel_driver");exit(-1);}
        }
    }
    
    int k=0;
        
    for(int i=0;i<size_inter;i++){
        interface_numero=Myconfig->interface[i].altsetting[0].bInterfaceNumber; 
        int status=libusb_claim_interface(Mydevice,interface_numero); // Réclamation de l'interface numero interface_numero
        if(status!=0)
        {perror("libusb_claim_interface"); exit(-1);}
        printf("Interface bien réclamée : Numero interface : %d\n\n",interface_numero);
        
        //Récuperation des endpoints
        int cpt=0;
        uint8_t nombre_endpoint=Myconfig->interface[i].altsetting[0].bNumEndpoints; //nombre d'endpoints
        printf("\t.....Nombre d'Endpoints de l'interface %d : %d\n\n",interface_numero,nombre_endpoint);
        
        for(int j=0;j<nombre_endpoint;j++){
            //On'récupère l'adresse et le type des endpoints. 
        uint8_t type_ep =Myconfig->interface[i].altsetting[0].endpoint[j].bmAttributes;     
        uint8_t endpoint_numero =Myconfig->interface[i].altsetting[0].endpoint[j].bEndpointAddress;
        
        if((type_ep & LIBUSB_TRANSFER_TYPE_MASK)==LIBUSB_TRANSFER_TYPE_INTERRUPT && cpt==0){ //on sélectionne ceux de type interruptions. 
        save[k].num_interface=interface_numero;
        save[k].num_interruptEp=endpoint_numero;
        k++;
        cpt=1;
        }
        printf("\t.....Recupération de l'Endpoint...... Indice de l'endpoint : %d : %d\n\n",endpoint_numero,j);
        }
        cpt=0;
        printf("/--------------------\n\n");
    }
    
    printf(">>>>>> Sauvergarde des Endpoints....\n");
    for(k=0; k<size_inter; k++)
        printf("\t.....Numero de l'interface %d : %d\n\t.....Numero du EndPoint %d : %d\n",k,save[k].num_interface,k,save[k].num_interruptEp);
}




//Dans notre programme pour Atmega328p:  Bouton D4='$' Bouton D3='"' Bouton D5='(' Bouton D6='0' et le Joystick='!'.

void Gestion_manette(struct libusb_device_handle* handle, struct Save * save){
  
    int timeout=0;  
    int size=1;                   
    unsigned char Button,Led;  //Pour les endpoints --> Button=endpoint_in, led=endpoint_out
    unsigned char etat[1]; //correspond à l'état des boutons, la donnée échangée
    int nb_bytes;  //le nombre de bits transférés.
    
    //Definition des points d'accès d'entrée et de sortie
    for(int i=0;i<2; i++){
        if((save[i].num_interruptEp&LIBUSB_ENDPOINT_IN)==LIBUSB_ENDPOINT_IN){
        Button=save[i].num_interruptEp; //Button car sens IN
        }
        else{
        Led=save[i].num_interruptEp; //Led car sens OUT
        } 
    }
    
    //Allumer la led
    //Reception des caractere depuis la manette : etat[0]='1' c'est pour allumer ou ='0' pour éteindre
    printf("\n>>>>>> LED ON <<<<<<\n");
    etat[0]='1';
    int status=libusb_interrupt_transfer(handle,Led,etat,size,&nb_bytes,timeout);
    if(status!=0){
        perror("libusb_interrupt_transfer");
    }
    
    //Gestion des boutons
    //On affiche les boutons pressés jusqu’à appui sur le bouton d’arrêt (joystick)
    printf("\nVeuillez presser un bouton quelconque du PAD (ou sur le Joystick pour quitter)\n");
    while(1){
        int status=libusb_interrupt_transfer(handle,Button,etat,size,&nb_bytes,timeout);
        if(status!=0){
        perror("libusb_interrupt_transfer");
        }
        else if(etat[0]!=0x20){
        printf("\n\tVous avez appuyé sur : 0x%x ==> %c \n",etat[0], etat[0]);  
        printf("\nVeuillez presser un bouton quelconque du PAD (ou sur le Joystick pour quitter)\n");
        }
        //à l'appui du Joystick la led  s'eteint et on quitte
        if(etat[0]=='!'){
        printf("\n\n<<<<<< LED OFF >>>>>>");
        etat[0]='0';
        status=libusb_interrupt_transfer(handle,Led,etat,size,&nb_bytes,timeout);
        if(status!=0){
        perror("libusb_interrupt_transfer");
        }
        break;
        }
    }
}


int main(){
  
  libusb_context *context;
  int status=libusb_init(&context);
  if(status!=0) {perror("libusb_init"); exit(-1);}
  
  libusb_device **list;//On récupère la poignée mode list
  ssize_t count=libusb_get_device_list(context,&list);
  if(count<0) {perror("libusb_get_device_list"); exit(-1);}
  
  ssize_t i=0;
  
  for(i=0;i<count;i++){
    libusb_device *device=list[i];
    struct libusb_device_descriptor desc;
    int status=libusb_get_device_descriptor(device,&desc);
    
    if(status!=0) continue;
    
    uint16_t vendor_id=0x2020;
    
    //On s'interesse uniquement à notre périphérique
    if(desc.idVendor == vendor_id){
      
      //Si trouvé, on l'ouvre
      printf("\nVendorID trouvé = %x \n",desc.idVendor);
      printf("ProductID trouvé = %x \n",desc.idProduct); 
      printf("\nOuverture du périphérique... Hello ! :\n");
      int status= libusb_open(device,&(Mydevice));
      if(status!=0){perror("libusb_open");exit(-1); }
      printf("\t.....Handle sauvegardé avec succès \n");
      
      //On le configure
      configuration();
      int size_inter = Myconfig->bNumInterfaces;
      struct Save save[size_inter]; //structure pour enregistrer les endpoints dans un tableau de taille le nombre d'interfaces
      interfaces(save,size_inter);   
      
      //Testons de la manette
      Gestion_manette(Mydevice,save);
      
      //On referme le périphérique
      fermeture(&Mydevice);
      
      //On récupère notre config descriptor
      status=libusb_get_active_config_descriptor(device,&Myconfig);
      if(status!=0){perror("libusb_get_active_config_descriptor");exit(-1);}
      
      libusb_free_config_descriptor(Myconfig);
    }
  }
  libusb_free_device_list(list,1); //On libère les interfaces
  libusb_exit(context); 
  return 0;  
}



